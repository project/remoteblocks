<?php

/**
 * Loads a remoteblock object.
 *
 * @param mixed $bid
 *    If $bid is int, then it will be used for the $bid.
 *    If $bid is string, then it will be used as a name.
 * @return stdClass
 */
function remoteblocks_load($identifier) {
  $remoteblock = db_fetch_object(is_numeric($identifier) ?
    db_query('SELECT * FROM {remote_blocks} WHERE bid = %d', $identifier):
    db_query('SELECT * FROM {remote_blocks} WHERE name = \'%s\'', $identifier));
  drupal_alter('remoteblock_load', $remoteblock);
  return $remoteblock;
}

/**
 * Loads all remoteblock objects.
 *
 * @return array
 */
function remoteblocks_load_all($byname = FALSE) {
  $ret = array();
  $res = db_query('SELECT * FROM {remote_blocks} ORDER BY ' .
    ($byname ? 'name' : 'bid'));
  while ($row = db_fetch_object($res)) {
    drupal_alter('remoteblock_load', $row);
    $ret []= $row;
  }
  return $ret;
}

/**
 * Saves a remoteblock object.
 *
 * @param stdClass $rb
 * @param bool $force_name_check
 * By setting this to TRUE, the function checks if the name is exists in the database.
 * @return boolean
 */
function remoteblocks_save($rb, $force_name_check = FALSE) {
  drupal_alter('remoteblock_presave', $rb);
  if ($force_name_check) {
    $nameexists = (bool) db_result(db_query(
      'SELECT COUNT(name) AS c FROM {remote_blocks} WHERE name = \'%s\'',
      $rb->name));
    $ret = drupal_write_record('remote_blocks', $rb, $nameexists ? 'name' : array());
  }
  else {
    $ret = drupal_write_record('remote_blocks', $rb, ($rb->bid > 0) ? 'bid' : array());
  }
  if ($ret) {
    module_invoke_all('remoteblock_save', $rb);
  }
  
  return $ret;
}

/**
 * Deletes a remoteblock object.
 *
 * @param mixed $identifier
 * @return boolean
 */
function remoteblocks_delete($identifier) {
  if (is_object($identifier)) {
    $identifier = $identifier->bid;
  }
  db_query((is_numeric($identifier) ?
      'DELETE FROM {remote_blocks} WHERE bid = %d':
      'DELETE FROM {remote_blocks} WHERE name = \'%s\''),
    $identifier);
  module_invoke_all('remoteblock_delete', $identifier);
  return (bool) db_affected_rows();
}

/**
 * Returns true if the URL is a valid remoteblock server.
 *
 * @param string $url
 * @return boolean
 */
function remoteblocks_check_url($url) {
  $response = drupal_http_request($url . '/remoteblocks');
  if (isset($response->data)) {
    $data = json_decode($response->data);
  }
  else {
    return FALSE;
  }
  return ($response->code == 200) && is_object($data) &&
    isset($data->type) && $data->type == 'error' &&
    isset($data->code) && $data->code == 400 ?
          $data->data :
          FALSE;
}

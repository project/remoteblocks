<?php

require_once 'remoteblocks.api.inc';

/**
 * Implementation of hook_perm().
 */
function remoteblocks_perm() {
  return array('access remote blocks', 'administer remote blocks');
}

/**
 * Implementation of hook_menu().
 */
function remoteblocks_menu() {
  $items = array();

  $items['admin/content/remoteblocks'] = array(
    'title' => 'Remote blocks',
    'description' => 'Adminster remote block settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('remoteblocks_admin_form'),
    'file' => 'remoteblocks.admin.inc',
  );

  $items['admin/content/remoteblocks/general'] = array(
    'title' => 'General',
    'type' => MENU_DEFAULT_LOCAL_TASK,
    'weight' => -99,
  );

  $items['admin/content/remoteblocks/list'] = array(
    'title' => 'List',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'remoteblocks_list',
    'weight' => -10,
  );

  $items['admin/content/remoteblocks/add'] = array(
    'title' => 'Add',
    'type' => MENU_LOCAL_TASK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('remoteblocks_add_form'),
  );

  $items['admin/content/remoteblocks/edit/%'] = array(
    'title' => 'Edit',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('remoteblocks_edit_form', 4),
  );

  $items['admin/content/remoteblocks/delete/%'] = array(
    'title' => 'Delete',
    'type' => MENU_CALLBACK,
    'page callback' => 'drupal_get_form',
    'page arguments' => array('remoteblocks_delete_form', 4),
  );

  foreach ($items as &$r) {
    $r['access arguments'] = array('administer remote blocks');
    $r['file'] = 'remoteblocks.admin.inc';
  }

  // END OF ADMIN SECTION //

  if (variable_get('remoteblocks_enable_server', FALSE)) {
    $items['remoteblocks'] = array(
      'title' => 'Remote block callback',
      'page callback' => 'rbserver',
      'access arguments' => array('access remote blocks'),
      'type' => MENU_CALLBACK,
      'file' => 'rbserver.inc',
    );
  }

  return $items;
}

/**
 * Implementation of hook_block().
 */
function remoteblocks_block($op = 'list', $delta = 0, $edit = array()) {
  switch ($op) {
    case 'list':
      $blocks = array();

      $rbs = remoteblocks_load_all();
      foreach ($rbs as $rb) {
        $blocks[$rb->bid] = array(
          'info' => 'Remote block: '.$rb->url.", {$rb->module}: " .
            (empty($rb->info) ? $rb->delta : $rb->info),
          'cache' => BLOCK_NO_CACHE,
        );
      }
      return $blocks;
      break;
    case 'view':
      drupal_add_js(drupal_get_path('module', 'remoteblocks').'/remoteblocks.js');
      $rb = remoteblocks_load($delta);
      if ($rb === FALSE) break;
      $settings = array(
        'url' => "{$rb->url}/remoteblocks/{$rb->module}/{$rb->delta}",
        'args' => array(),
      );
      drupal_alter('remoteblocks_js', $settings, $rb);
      $settings = json_encode($settings);
      drupal_add_js("
        Drupal.remoteBlocks.data[{$rb->bid}] = {$settings};
      ", 'inline', 'footer');
      $block = array();
      $block['subject'] = t('Loading...');
      $block['content'] = t('Loading...');
      return $block;
      break;
    case 'configure':
      break;
    case 'save':
      break;
  }
}

/**
 * Implementation of hook_remoteblock_access().
 */
function remoteblocks_remoteblock_access() {
  return array(
    'rbserver_anonymous_access' => array(
      'title' => t('Allow access to blocks which are visible to anonymous.'),
      'file' => array('inc', 'remoteblocks', 'rbserver'),
    ),
    'rbserver_access' => array(
      'title' => t('Use the Drupal\'s user access system.'),
      'file' => array('inc', 'remoteblocks', 'rbserver'),
    ),
    'rbserver_full_access' => array(
      'title' => t('Provide full access to all blocks.'),
      'file' => array('inc', 'remoteblocks', 'rbserver'),
    ),
  );
}

/**
 * Implements hook_features_api().
 */
function remoteblocks_features_api() {
  return array(
    'remoteblock' => array(
      'name' => t('Remote blocks'),
      'default_hook' => 'remoteblock_defaults',
      'default_file' => FEATURES_DEFAULTS_INCLUDED,
      'features_source' => TRUE,
      'file' => drupal_get_path('module', 'remoteblocks') .
                '/remoteblocks.features.inc',
    ),
  );
}

/**
 * Return all blocks in the specified region for the current user.
 *
 * This function is copypasted from block.inc, and modified.
 *
 * @param $region
 *   The name of a region.
 *
 * @return
 *   An array of block objects, indexed with <i>module</i>_<i>delta</i>.
 *   If you are displaying your blocks in one or two sidebars, you may check
 *   whether this array is empty to see how many columns are going to be
 *   displayed.
 *
 * @todo
 *   Now that the blocks table has a primary key, we should use that as the
 *   array key instead of <i>module</i>_<i>delta</i>.
 */
function local_block_list($region) {
  global $user, $theme_key;

  static $blocks = array();

  if (!count($blocks)) {
    $rids = array_keys($user->roles);
    $result = db_query(db_rewrite_sql("SELECT DISTINCT b.*
        FROM {blocks} b
        LEFT JOIN {blocks_roles} r ON b.module = r.module AND b.delta = r.delta
        WHERE b.theme = '%s' AND
          (r.rid IN (". db_placeholders($rids) .") OR r.rid IS NULL)
        ORDER BY b.region, b.weight, b.module", 'b', 'bid'),
      array_merge(array($theme_key), $rids));
    while ($block = db_fetch_object($result)) {
      if (!isset($blocks[$block->region])) {
        $blocks[$block->region] = array();
      }
      // Use the user's block visibility setting, if necessary
      if ($block->custom != 0) {
        if ($user->uid && isset($user->block[$block->module][$block->delta])) {
          $enabled = $user->block[$block->module][$block->delta];
        }
        else {
          $enabled = ($block->custom == 1);
        }
      }
      else {
        $enabled = TRUE;
      }

      // Match path if necessary
      if ($block->pages) {
        if ($block->visibility < 2) {
          $path = drupal_get_path_alias($_GET['q']);
          // Compare with the internal and path alias (if any).
          $page_match = drupal_match_path($path, $block->pages);
          if ($path != $_GET['q']) {
            $page_match = $page_match || drupal_match_path($_GET['q'], $block->pages);
          }
          // When $block->visibility has a value of 0, the block is displayed on
          // all pages except those listed in $block->pages. When set to 1, it
          // is displayed only on those pages listed in $block->pages.
          $page_match = !($block->visibility xor $page_match);
        }
        else {
          $page_match = drupal_eval($block->pages);
        }
      }
      else {
        $page_match = TRUE;
      }
      $block->enabled = $enabled;
      $block->page_match = $page_match;
      $blocks[$block->region]["{$block->module}_{$block->delta}"] = $block;
    }
  }

  // Create an empty array if there were no entries
  if (!isset($blocks[$region])) {
    $blocks[$region] = array();
  }

  foreach ($blocks[$region] as $key => $block) {
    // Render the block content if it has not been created already.
    if (!isset($block->content)) {
      // Erase the block from the static array - we'll put it back if it has content.
      unset($blocks[$region][$key]);
      if ($block->enabled && $block->page_match) {
        // Check the current throttle status and see if block should be displayed
        // based on server load.
        if (!($block->throttle && (module_invoke('throttle', 'status') > 0))) {
          // Try fetching the block from cache. Block caching is not compatible with
          // node_access modules. We also preserve the submission of forms in blocks,
          // by fetching from cache only if the request method is 'GET'.
          if (!count(module_implements('node_grants')) &&
              $_SERVER['REQUEST_METHOD'] == 'GET' &&
              ($cid = _block_get_cache_id($block)) &&
              ($cache = cache_get($cid, 'cache_block'))) {
            $array = $cache->data;
          }
          else {
            $array = module_invoke($block->module, 'block', 'view', $block->delta);
            if (isset($cid)) {
              cache_set($cid, $array, 'cache_block', CACHE_TEMPORARY);
            }
          }

          if (isset($array) && is_array($array)) {
            foreach ($array as $k => $v) {
              $block->$k = $v;
            }
          }
        }
        if (isset($block->content) && $block->content) {
          // Override default block title if a custom display title is present.
          if ($block->title) {
            // Check plain here to allow module generated titles to keep any markup.
            $block->subject = $block->title == '<none>' ? '' : check_plain($block->title);
          }
          if (!isset($block->subject)) {
            $block->subject = '';
          }
          $blocks[$block->region]["{$block->module}_{$block->delta}"] = $block;
        }
      }
    }
  }
  return $blocks[$region];
}

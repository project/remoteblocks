Drupal.remoteBlocks = {};

Drupal.remoteBlocks.data = {};

Drupal.remoteBlocks.getFullURL = function(bid) {
  var url = Drupal.remoteBlocks.data[bid].url + '?callback=?';
  for (var i in Drupal.remoteBlocks.data[bid].args) {
    url += '&' + i + '=' + Drupal.remoteBlocks.data[bid].args[i];
  }

  return url;
};

Drupal.behaviors.remoteBlocks = function(context) {
  try {
    console;
  }
  catch (ex) {
    console = {};
    console.log = function() {};
  }
  $("div.block-remoteblocks").each(function() {
    var bid = $(this).attr('id').match(/[0-9]*$/)[0];
    $.getJSON(Drupal.remoteBlocks.getFullURL(bid), function(data){
      $("#block-remoteblocks-" + bid + " h2").html(data.subject || '');
      $("#block-remoteblocks-" + bid + " div.content").html(data.content || '');
    });
  });
  Drupal.remoteBlocks.formPrepare($("#remoteblocks-add-form"));
  Drupal.remoteBlocks.formPrepare($("#remoteblocks-edit-form"));
  $("#remoteblocks-edit-form #edit-url").trigger('blur');
}

Drupal.remoteBlocks.urldata = {};

Drupal.remoteBlocks.formPrepare = function(form) {
  $("#edit-module", form).hide();
  $("#edit-delta", form).hide();
  form.submit(function() {
    $("#edit-module").val($("#edit-module-select").val());
    $("#edit-delta").val($("#edit-delta-select").val());
  });
  $("#edit-url", form).blur(function() {
    $("#edit-url", form).removeClass('error');
    $('#edit-delta-select').remove();
    $('#edit-module-select').remove();
    $.ajax({
      url: $(this).val() + '/remoteblocks?callback=?',
      dataType: 'json',
      success: function(data) {
        console.log(data);
        if (data.type != 'error' || data.code != 400) {
          return;
        }
        Drupal.remoteBlocks.urldata = data;
        $('#edit-delta', form).after($('<select/>').attr('id', 'edit-delta-select'));
        var moduleSelect = $('<select/>').attr('id', 'edit-module-select');
        for (var module in data.data) {
          if (data.data[module].length == 0) continue;
          var opt = $('<option/>')
            .attr('value', module)
            .html(module);
          if ($("#edit-module", form).val() == module) {
            opt.attr('selected', 'selected');
          }
          moduleSelect.append(opt);
        }
        $('#edit-module', form).after(moduleSelect);
        $('#edit-module-select').change(function() {
          $("#edit-delta-select", form).html('');
          for(var delta in Drupal.remoteBlocks.urldata.data[$(this).val()]) {
            var opt = $('<option/>')
              .attr('value', delta)
              .html(Drupal.remoteBlocks.urldata.data[$(this).val()][delta].info);
            if ($("#edit-delta", form).val() == delta) {
              opt.attr('selected', 'selected');
            }
            $("#edit-delta-select", form).append(opt);
          }
        }).trigger('change');
      },
      error: function(request) {
        console.log(request);

        if ($('div.messages.error').length == 0) {
          $("#tabs-wrapper").after($('<div/>')
            .addClass('messages')
            .addClass('error')
            .append($('<ul/>')));
        }

        $('div.messages.error ul').append($('<li/>').html(Drupal.t(request.statusText)));
      },
      beforeSend: function() {
        $('div.messages.error').remove();
      }
    });
  });
}
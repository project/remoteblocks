<?php

/**
 * Menu callback for the remote block server settings form.
 */
function remoteblocks_admin_form() {
  $f = array();

  $f['remoteblocks_enable_server'] = array(
    '#type' => 'checkbox',
    '#default_value' => variable_get('remoteblocks_enable_server', FALSE),
    '#title' => t('Enable server'),
  );

  $access_options = array();
  foreach(module_invoke_all('remoteblock_access') as $callback => $info) {
    $access_options[$callback] = $info['title'];
  }

  $f['remoteblocks_check_access'] = array(
    '#type' => 'radios',
    '#default_value' =>
      variable_get('remoteblocks_check_access', 'rbserver_anonymous_access'),
    '#options' => $access_options,
    '#title' => t('Access check'),
  );

  $f = system_settings_form($f);
  
  // Need to explicitly put in our submit handler when using system_settings_form().
  $f['#submit'][] = 'remoteblocks_admin_form_submit';  

  return $f;
}

/**
 * Form submit handler for the remote block server settings form.
 */
function remoteblocks_admin_form_submit() {
  // Clear the menu cache as a change of settings adds or removes a menu path.
  menu_rebuild();
}

/**
 * Menu callback for the remote block list admin page.
 */
function remoteblocks_list() {
  $ops = module_invoke_all('remoteblocks_list_ops');

  $header = array(
    t('URL'),
    t('Name'),
    t('Module'),
    t('Delta'),
    t('Title'),
    array('data' => t('Operations'), 'colspan' => 2 + count($ops)),
  );
  $rows = array();
  foreach (remoteblocks_load_all() as $row) {
    $r = array(
      $row->url,
      $row->name,
      $row->module,
      $row->delta,
      empty($row->info) ? '' : $row->info,
      l(t('Edit'), 'admin/content/remoteblocks/edit/' . $row->bid),
      l(t('Delete'), 'admin/content/remoteblocks/delete/' . $row->bid),
    );

    foreach ($ops as $op) {
      $r = array_merge($r, $op($row->bid));
    }

    $rows[] = $r;
  }

  return theme('table', $header, $rows);
}

/**
 * Form callback for the regblocks add form.
 */
function remoteblocks_add_form($form) {
  drupal_add_js(drupal_get_path('module', 'remoteblocks').'/remoteblocks.js');

  $f = array();

  $f['url'] = array(
    '#type' => 'textfield',
    '#title' => t('URL'),
    '#description' =>
      t("The base URL of the remote site providing this block. Example: 'http://example.com'."),
    '#required' => TRUE,
  );

  $f['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#description' => t('Must be unique. Can contain letters and underscores.'),
    '#required' => TRUE,
  );

  $f['module'] = array(
    '#type' => 'textfield',
    '#title' => t('Module'),
    '#required' => TRUE,
  );

  $f['delta'] = array(
    '#type' => 'textfield',
    '#title' => t('Delta'),
    '#required' => FALSE,
    '#default_value' => '0',
  );

  $f['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create'),
  );

  return $f;
}

/**
 * Validation callback for remoteblocks_add_form.
 */
function remoteblocks_add_form_validate($form, &$form_state) {
  $result = remoteblocks_check_url($form_state['values']['url']);
  if ($result === FALSE) {
    form_set_error('url', t('Invalid server'));
  }
  else {
    $form_state['storage'] = $result;
  }

  if (!preg_match('/^[a-z_]*$/', $form_state['values']['name'])) {
    form_set_error('name', t('Invalid name'));
  }
}

/**
 * Submit callback for remoteblocks_add_form.
 */
function remoteblocks_add_form_submit($form, &$form_state) {
  $result = remoteblocks_save((object) array(
    'url' => $form_state['values']['url'],
    'name' => $form_state['values']['name'],
    'bid' => isset($form_state['values']['bid']) ? $form_state['values']['bid'] : NULL,
    'module' => $form_state['values']['module'],
    'delta' => $form_state['values']['delta'],
    'info' => (isset($form_state['storage']->{$form_state['values']['module']}->{$form_state['values']['delta']}->info) ?
                $form_state['storage']->{$form_state['values']['module']}->{$form_state['values']['delta']}->info : ''),
  ));
  drupal_goto('admin/content/remoteblocks/list');
  drupal_set_message(t('Server saved.'));
}

/**
 * Form callback for the regblocks edit form.
 */
function remoteblocks_edit_form($form, $bid) {
  $f = remoteblocks_add_form(null);
  $rb = remoteblocks_load($bid);

  $f['url']['#default_value'] = $rb->url;
  $f['module']['#default_value'] = $rb->module;
  $f['delta']['#default_value'] = $rb->delta;
  $f['name']['#default_value'] = $rb->name;

  $f['submit']['#value'] = t('Update');

  $f['bid'] = array(
    '#type' => 'value',
    '#value' => $bid,
  );

  return $f;
}

/**
 * Validation callback for remoteblocks_edit_form.
 */
function remoteblocks_edit_form_validate($form, &$form_state) {
  remoteblocks_add_form_validate($form, $form_state);
}

/**
 * Submit callback for remoteblocks_edit_form.
 */
function remoteblocks_edit_form_submit($form, &$form_state) {
  remoteblocks_add_form_submit($form, $form_state);
}

/**
 * Form callback for the regblocks delete form.
 */
function remoteblocks_delete_form($form, $bid) {
  $f = array();

  $rb = remoteblocks_load($bid);

  $f['bid'] = array(
    '#type' => 'value',
    '#value' => $bid,
  );

  return confirm_form($f,
    t('Are you sure that you want to delete %url?',
      array('%url' => $rb->url.", {$rb->module}: " . (empty($rb->info) ? $rb->delta : $rb->info))),
    (isset($_GET['destination']) ? $_GET['destination'] : 'admin/content/remoteblocks/list'),
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit callback for remoteblocks_delete_form.
 */
function remoteblocks_delete_form_submit($form, &$form_state) {
  remoteblocks_delete($form_state['values']['bid']);
  $form_state['redirect'] = 'admin/content/remoteblocks/list';
  cache_clear_all(); // TODO make this a bit more nicer
}

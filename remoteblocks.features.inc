<?php

/**
 * Implementation of hook_features_export().
 * 
 * This is a component hook.
 */
function remoteblock_features_export($data, &$export, $module_name) {
  $export['dependencies']['features'] = 'features';
  $export['dependencies']['remoteblocks'] = 'remoteblocks';
  foreach ($data as $rbname) {
    $export['features']['remoteblock'][$rbname] = $rbname;

    foreach (module_implements('remoteblocks_load_alter') as $module) {
      $export['dependencies'][$module] = $module;
    }
  }

  return array();
}

/**
 * Implementation of hook_features_export_options().
 * 
 * This is a component hook.
 */
function remoteblock_features_export_options() {
  $options = array();

  foreach (remoteblocks_load_all() as $rb) {
    $options[$rb->name] = $rb->name;
  }

  return $options;
}

/**
 * Implementation of hook_features_render().
 * 
 * This is a component hook.
 */
function remoteblock_features_export_render($module_name, $data) {
  $code = array();

  $code[] = '  $remoteblocks = array();';

  foreach ($data as $name) {
    $rb = remoteblocks_load($name);
    unset($rb->bid);
    $code[] = "  \$remoteblocks['{$name}'] = " .
      features_var_export($rb, '  ') . ';';
  }

  $code[] = '  return $remoteblocks;';

  return array('remoteblock_defaults' => implode("\n", $code));
}

/**
 * Implementation of hook_features_revert().
 * 
 * This is a component hook.
 */
function remoteblock_features_revert($module_name) {
//  $rbdefs = features_get_default('remoteblock', $module_name);
//  if (!empty($rbdefs)) {
//    foreach ($rbdefs as $rbdef) {
//      remoteblocks_delete($rbdef->name);
//    }
//  }
  remoteblock_features_rebuild($module_name);
}

/**
 * Implementation of hook_features_rebuild().
 * 
 * This is a component hook.
 */
function remoteblock_features_rebuild($module_name) {
  $rbdefs = features_get_default('remoteblock', $module_name);
  if (!empty($rbdefs)) {
    foreach ($rbdefs as $rbdef) {
      remoteblocks_save($rbdef, TRUE);
    }
  }
}


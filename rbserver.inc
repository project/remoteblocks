<?php

/**
 * Assembles an error object and returns the json representation.
 *
 * @param int $code
 * @param string $message
 * @param mixed $data
 * @return json
 */
function rbserver_error($code, $message = '', $data = NULL) {
  return json_encode(array(
    'type' => 'error',
    'code' => $code,
    'message' => $message,
    'data' => $data,
  ));
}

/**
 * Returns a list of all blocks, which are visible to the current user.
 *
 * @return array
 */
function rbserver_get_all_blocks() {
  $cache = &ctools_static(__FUNCTION__, NULL);

  if ($cache === NULL) {
    $cache = array();
    $access_callback = rbserver_get_access_callback();
    foreach (module_implements("block") as $module) {
      $function = $module.'_block';
      $cache[$module] = array();
      foreach (call_user_func($function, 'list') as $delta => $description) {
        if ($access_callback($module, $delta, TRUE)) {
          $cache[$module][$delta] = $description;
        }
      }
    }
  }
  return $cache;
}

/**
 * Returns the access callback for the blocks.
 *
 * @return string
 */
function rbserver_get_access_callback() {
  $cache = &ctools_static(__FUNCTION__, NULL);

  if ($cache === NULL) {
    $hookdata = module_invoke_all('remoteblock_access');
    $callback = variable_get('remoteblocks_check_access', 'rbserver_anonymous_access');

    if (!empty($hookdata[$callback]['file']) && is_array($hookdata[$callback]['file'])) {
      call_user_func_array('module_load_include', $hookdata[$callback]['file']);
    }

    $cache = $callback;
  }

  return $cache;
}

/**
 * Implementation of the Drupal access system for the remote blocks.
 *
 * @todo rewrite to get the correct access information for the non-visible blocks
 * @global string $theme_key
 * @param string $module
 * @param string $delta
 * @param boolean $list
 * @return boolean
 */
function rbserver_access($module, $delta, $list = FALSE) {
  global $user;
  global $theme_key;
  $cache = &ctools_static(__FUNCTION__, array());

  if ($user->uid == 1) {
    return TRUE;
  }

  if ($list) {
    return TRUE;
  }

  if (!isset($cache[$module])) {
    $cache[$module] = array();
  }

  if (!isset($cache[$module][$delta])) {
    $cache[$module][$delta] = FALSE;

    init_theme();
    $block_regions = system_region_list($theme_key) +
      array('' => '<'. t('none') .'>');

    foreach ($block_regions as $block_region => $block_region_name) {
      $blocks = local_block_list($block_region);
      if (isset($blocks["{$module}_{$delta}"])) {
        $cache[$module][$delta] = TRUE;
      }
    }
  }

  return $cache[$module][$delta];
}

/**
 * Implementation of a basic access system for the remote blocks.
 *
 * This access system enables to see just the public blocks which
 * are visible to anonymous.
 *
 * @global string $theme_key
 * @param string $module
 * @param string $delta
 * @param boolean $list
 * @return boolean
 */
function rbserver_anonymous_access($module, $delta, $list = FALSE) {
  // Check access to the requested block.
  // We have to call init_theme() ahead of time to get the current theme_key.
  init_theme();

  // Cribbed from block_list().
  global $user, $theme_key;
  $rids = array_keys($user->roles);
  $result = db_query(db_rewrite_sql("SELECT DISTINCT b.*
    FROM {blocks} b
    LEFT JOIN {blocks_roles} r ON b.module = r.module AND b.delta = r.delta
    WHERE b.theme = '%s' AND
      b.module = '%s' AND
      b.delta = '%s' AND
      b.status = 1 AND
      (r.rid IN (". db_placeholders($rids) .") OR r.rid IS NULL)", 'b', 'bid'),
    array_merge(array($theme_key, $module, $delta), $rids));

  return (bool) db_fetch_object($result);
}

/**
 * Implementation of a constant TRUE access system.
 *
 * @return boolean
 */
function rbserver_full_access() {
  return TRUE;
}

/**
 * Page callback for 'remoteblocks'.
 */
function rbserver() {
  $args = func_get_args();

  module_invoke_all('rbserver_boot', $args);

  if (count($args) < 1) {
    rbserver_print(rbserver_error(400, t('Invalid arguments'), rbserver_get_all_blocks()));
  }
  else {
    $module = $args[0];
    $delta = isset($args[1]) ? $args[1] : 0;

    $access_callback = rbserver_get_access_callback();

    if ($access_callback($module, $delta)) {
      $package = module_invoke_all('rbserver_respond', $args);
      if (!is_array($package)) {
        $package = array();
      }
      $block = module_invoke($module, 'block', 'view', $delta);
      if (!is_array($block)) {
        watchdog('remoteblocks blockerror', serialize(array(
          'block' => $block,
          'module' => $module,
          'delta' => $delta,
        )));
        $block = (array) $block;
      }
      $package = array_merge($package, $block);
      drupal_alter('rbserver_package', $package, $args);
      rbserver_print(json_encode($package));
    }
    else {
      rbserver_print(rbserver_error(403, t('Permission denied')));
    }
  }

  module_invoke_all('rbserver_shutdown');

  return NULL;
}

/**
 * Prints json.
 *
 * This function is aware to jsonp.
 *
 * @param json $json
 */
function rbserver_print($json) {
  print empty($_GET['callback']) ? $json : "{$_GET['callback']}($json);";
}
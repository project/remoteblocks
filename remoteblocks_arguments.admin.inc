<?php

/**
 * Page callback for 'admin/content/remoteblocks/arguments/%remoteblocks'.
 */
function remoteblocks_arguments_list_page($rb) {
  $header = array(
    t('Type'),
    t('Value'),
    array('data' => t('Operations'), 'colspan' => 1),
  );

  $rows = array();
  $res = db_query('SELECT type, replacement FROM {remote_blocks_arguments}
      WHERE bid = %d ORDER BY type',
    $rb->bid);
  while ($row = db_fetch_object($res)) {
    $rows[] = array(
      $row->type,
      $row->replacement,
      l(t('Delete'), "admin/content/remoteblocks/arguments/{$rb->bid}/{$row->type}/delete"),
    );
  }

  return theme('table', $header, $rows);
}

/**
 * Form callback on 'admin/content/remoteblocks/arguments/%remoteblocks/add'.
 */
function remoteblocks_arguments_add_form(&$form_state, $rb) {
  $f = array();

  if (!isset($form_state['storage'])) {
    $form_state['storage'] = array(
      'step' => 0,
      'type' => NULL,
      'rb' => $rb,
    );
  }

  if (empty($form_state['storage']['hookdata'])) {
    $form_state['storage']['hookdata'] =
      remoteblocks_arguments_get_info();
  }

  switch ($form_state['storage']['step']) {
    case 0:
      $options = array();
      foreach ($form_state['storage']['hookdata'] as $type => $data) {
        $options[$type] = $data['title'];
      }

      $f['type'] = array(
        '#title' => t('Argument type'),
        '#type' => 'radios',
        '#options' => $options,
      );
      break;
    case 1:
      $type = $form_state['storage']['type'];
      $callback = $form_state['storage']['hookdata'][$type]['widget_callback'];
      $f['replacement'] = $callback($f, $form_state, NULL);
      break;
  }

  if ($form_state['storage']['step'] > 0) {
    $f['previous'] = array(
      '#type' => 'submit',
      '#value' => t('Previous'),
    );
  }

  $f['next'] = array(
    '#type' => 'submit',
    '#value' => t('Next'),
  );

  return $f;
}

/**
 * Submit callback for 'remoteblocks_arguments_add_form'.
 */
function remoteblocks_arguments_add_form_submit($form, &$form_state) {
  switch ($form_state['storage']['step']) {
    case 0:
      $form_state['rebuild'] = TRUE;
      $form_state['storage']['type'] = $form_state['values']['type'];
      break;
    case 1:
      $rb = $form_state['storage']['rb'];
      $rb->arguments[$form_state['storage']['type']] =
        $form_state['values']['replacement'];

      remoteblocks_save($rb);
      drupal_goto('admin/content/remoteblocks/arguments/' . $rb->bid);
      break;
  }
  if ($form_state['clicked_button']['#value'] == t('Previous')) {
    $form_state['storage']['step']--;
  }
  else {
    $form_state['storage']['step']++;
  }
}

/**
 * Form callback on 'admin/content/remoteblocks/arguments/%remoteblocks/%/delete'.
 */
function remoteblocks_arguments_delete_form($form_state, $rb, $arg) {
  $f = array();

  $f['rb'] = array(
    '#type' => 'value',
    '#value' => $rb,
  );

  $f['arg'] = array(
    '#type' => 'value',
    '#value' => $arg,
  );
  
  return confirm_form($f,
    t('Are you sure that you want to delete that argument?'),
    'admin/content/remoteblocks/arguments/' . $rb->bid);
}

/**
 * Submit callback for 'remoteblocks_arguments_delete_form'.
 */
function remoteblocks_arguments_delete_form_submit($form, &$form_state) {
  db_query('DELETE FROM {remote_blocks_arguments} WHERE bid = %d AND type = \'%s\'',
    $form_state['values']['rb']->bid, $form_state['values']['arg']);
  drupal_goto('admin/content/remoteblocks/arguments/' . $form_state['values']['rb']->bid);
}
